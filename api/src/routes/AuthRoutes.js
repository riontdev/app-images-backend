import { Router } from 'express';
import jwt from 'jsonwebtoken';
import passportFacebook from '../config/facebook';
import passportGoogle from '../config/google';
import passportLocal from '../config/jwt';
import Util from '../utils/Utils';

const util = new Util();
const router = new Router();

// TODO REFACTORING CODE...
router.post('/local',
  passportLocal.authenticate('local', { session: false }), (req, res) => {
    const { user } = req;
    req.login(user, { session: false }, (err) => {
      if (err) {
        util.setError(403, err);
      }
      /* generate a signed son web token with the contents
      of user object and return it in the response */
      const token = jwt.sign({ id: req.user.id }, process.env.SECRET_KEY || 'secret-key');
      util.setSuccess(200, 'Login Success', { token });
      return util.send(res);
    });
  });
/* FACEBOOK ROUTER */
router.get('/facebook', (req, res, next) => {
  if (req.query.return) {
    req.session.oauth2return = req.query.return;
  }
  next();
},
passportFacebook.authenticate('facebook'));

router.get('/facebook/callback',
  passportFacebook.authenticate('facebook', { failureRedirect: '/auth/local' }),
  (req, res) => {
    // Successful authentication, redirect home.
    res.redirect('/');
  });

/* GOOGLE ROUTER */
router.get('/google',
  passportGoogle.authenticate('google', { scope: ['email', 'profile'] }), (req, res) => util.send(res));

router.get('/google/callback',
  passportGoogle.authenticate('google', { failureRedirect: '/auth/local' }),
  (req, res) => {
    const token = jwt.sign({ id: req.user.id }, process.env.SECRET_KEY || 'secret-key');
    util.setSuccess(200, 'Login Success', { token });
    return util.send(res);
  });

router.get('/user', passportLocal.authenticate('jwt', { session: false }),
  (req, res) => {
    util.setSuccess(200, 'Sucess', req.user);
    return util.send(res);
  });

module.exports = router;
