import { Router } from 'express';
import multer from 'multer';
import ProductController from '../controllers/ProductController';
import passport from '../config/jwt';

const storage = multer.memoryStorage();

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  }
  else {
    // rejects storing a file
    cb(null, false);
  }
};

const upload = multer({
  storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter
});

const router = Router();
router.use(passport.authenticate('jwt', { session: false }));

router.get('/', ProductController.getAllProducts);
router.post('/', upload.single('file'), ProductController.addProduct);
router.get('/:id', ProductController.getAProduct);
router.put('/:id', upload.single('file'), ProductController.updatedProduct);
router.delete('/:id', ProductController.deleteProduct);

export default router;
