import admin from 'firebase-admin';
import { format } from 'util';

admin.initializeApp({
  credential: admin.credential.cert({
    projectId: process.env.FIREBASE_PROJECT_ID,
    private_key: process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
    client_email: process.env.FIREBASE_CLIENT_EMAIL
  }),
  storageBucket: process.env.BUCKET_URL
});

const bucket = admin.storage().bucket();

export default class Storage {
  static uploadImageToStorage(file, path) {
    return new Promise((resolve) => {
      if (!file) {
        Error('No image file');
      }
      const fileUpload = bucket.file(path);

      const blobStream = fileUpload.createWriteStream({
        metadata: {
          contentType: file.mimetype
        }
      });

      blobStream.on('error', () => {
        Error('Something is wrong! Unable to upload at the moment.');
      });

      blobStream.on('finish', () => {
        // The public URL can be used to directly access the file via HTTP.
        fileUpload.makePublic();
        const url = this.urlBucket(fileUpload.name);
        resolve(url);
      });

      blobStream.end(file.buffer);
    });
  }

  static urlBucket(filename) {
    return format(`https://storage.googleapis.com/${bucket.name}/${filename}`);
  }
}
