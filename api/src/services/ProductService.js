import database from '../models';
import storage from '../utils/Storage';

const pathProducts = 'users/products/';

class ProductService {
  static async getAllProducts(limit, offset) {
    try {
      return await database.Product.findAll({
        where: {
          user_id: 1
        },
        limit,
        offset
      });
    }
    catch (error) {
      throw error;
    }
  }

  static async addProduct(newProduct, file) {
    try {
      // set custom name for file
      const product = await database.Product.create(newProduct);
      const bucketPath = pathProducts + newProduct.photo;
      await storage.uploadImageToStorage(file, bucketPath);
      return product;
    }
    catch (error) {
      throw error;
    }
  }

  static async updateProduct(id, updateProduct, file) {
    try {
      const ProductToUpdate = await database.Product.findOne({
        where: { id: Number(id) }
      });

      if (ProductToUpdate) {
        // set custom name for file
        const product = await database.Product.update(updateProduct, { where: { id: Number(id) } });
        if (!file) {
          const bucketPath = pathProducts + product.photo;
          await storage.uploadImageToStorage(file, bucketPath);
        }

        return product;
      }
      return null;
    }
    catch (error) {
      throw error;
    }
  }

  static async getAProduct(id) {
    try {
      const theProduct = await database.Product.findOne({
        attributes: { exclude: ['id', 'user_id'] },
        where: { id: Number(id) }
      });

      return theProduct;
    }
    catch (error) {
      throw error;
    }
  }

  static async deleteProduct(id) {
    try {
      const ProductToDelete = await database.Product.findOne({ where: { id: Number(id) } });

      if (ProductToDelete) {
        const deletedProduct = await database.Product.destroy({
          where: { id: Number(id) }
        });
        return deletedProduct;
      }
      return null;
    }
    catch (error) {
      throw error;
    }
  }
}

export default ProductService;
