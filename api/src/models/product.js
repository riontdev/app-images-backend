import storage from '../utils/Storage';

module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    photo: {
      type: DataTypes.STRING,
      get() {
        return storage.urlBucket(`users/products/${this.getDataValue('photo')}`);
      }
    }
  }, {});
  Product.associate = (models) => {
    // associations can be defined here
    Product.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
  };
  return Product;
};
