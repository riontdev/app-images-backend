import path from 'path';
import ProductService from '../services/ProductService';
import Util from '../utils/Utils';

const util = new Util();

class ProductController {
  static async getAllProducts(req, res) {
    try {
      const limit = !req.query.limit || !Number(req.query.limit) ? 100 : req.query.limit;
      const offset = !req.query.offset || !Number(req.query.offset) ? 0 : req.query.offset;
      const allProducts = await ProductService.getAllProducts(limit, offset);
      if (allProducts.length > 0) {
        util.setSuccess(200, 'Products retrieved', allProducts);
      }
      else {
        util.setSuccess(200, 'No Product found');
      }
      return util.send(res);
    }
    catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async addProduct(req, res) {
    const { file } = req;
    if (!req.body.name || !req.file) {
      util.setError(400, 'Please provide name and file');
      return util.send(res);
    }
    try {
      const newProduct = {
        name: req.body.name,
        user_id: 1,
        photo: `${req.body.name}-${Date.now()}${path.extname(file.originalname)}`
      };
      const createdProduct = await ProductService.addProduct(newProduct, file);
      util.setSuccess(201, 'Product Added!', createdProduct);
      return util.send(res);
    }
    catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  static async updatedProduct(req, res, file) {
    const alteredProduct = req.body;
    if (req.file) {
      alteredProduct.photo = `${req.body.name}-${Date.now()}${path.extname(file.originalname)}`;
    }
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    try {
      const updateProduct = await ProductService.updateProduct(id, alteredProduct, file);
      if (!updateProduct) {
        util.setError(404, `Cannot find Product with the id: ${id}`);
      }
      else {
        util.setSuccess(200, 'Product updated', updateProduct);
      }
      return util.send(res);
    }
    catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getAProduct(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const theProduct = await ProductService.getAProduct(id);

      if (!theProduct) {
        util.setError(404, `Cannot find Product with the id ${id}`);
      }
      else {
        util.setSuccess(200, 'Found Product', theProduct);
      }
      return util.send(res);
    }
    catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async deleteProduct(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(res);
    }

    try {
      const ProductToDelete = await ProductService.deleteProduct(id);

      if (ProductToDelete) {
        util.setSuccess(200, 'Product deleted');
      }
      else {
        util.setError(404, `Product with the id ${id} cannot be found`);
      }
      return util.send(res);
    }
    catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
}

export default ProductController;
