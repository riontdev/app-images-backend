import UserService from '../services/UserService';
import Util from '../utils/Utils';

const util = new Util();

class UserController {
  static async addUser(req, res) {
    if (!req.body.email || !req.body.password) {
      util.setError(400, 'Please provide complete details');
      return util.send(res);
    }
    const newUser = req.body;
    try {
      const createdBook = await UserService.addUser(newUser);
      util.setSuccess(201, 'User Added!', createdBook);
      return util.send(res);
    }
    catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }
}

export default UserController;
