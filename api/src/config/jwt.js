import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';

import db from '../models';

const passportJWT = require('passport-jwt');

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
((email, password, cb) =>

// Assume there is a DB module pproviding a global UserModel
  db.User.findOne({ where: { email } })
    .then((user) => {
      if (!user && user.validPassword()) {
        return cb(null, false, { message: 'Incorrect email or password.' });
      }

      return cb(null, user, {
        message: 'Logged In Successfully'
      });
    })
    .catch((err) => cb(err))
)));

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.SECRET_KEY || 'secret-key'
},
((jwtPayload, cb) => 
  // find the user in db if needed
   db.User.findOne({ attributes: { exclude: ['id', 'password'] }, where: { id: jwtPayload.id } })
    .then((user) => cb(null, user))
    .catch((err) => cb(err))
)));

export default passport;
