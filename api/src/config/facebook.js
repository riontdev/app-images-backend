import passport from 'passport';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import db from '../models';

passport.use(new FacebookStrategy({
  clientID: process.env.FB_CLIENT_ID,
  clientSecret: process.env.FB_CLIENT_SECRET,
  callbackURL: process.env.FB_CALLBACK,
  profileFields: ['id', 'displayName', 'photos', 'email'],
  passReqToCallback: true
},
(accessToken, refreshToken, profile, done) => {
  // TODO change this block and user services...
  db.User.findOne({ where: { email: profile.emails[0].value } }).then((userFind) => {
    if (!userFind) {
      db.User.findOne({ where: { email: profile.emails[0].value } })
        .then((userCreated) => done(null, userCreated));
    }
    return done(null, userFind);
  }).then((err) => done(err));
}));

module.exports = passport;
