import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import passport from 'passport';
import db from '../models';

require('dotenv').config();

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: process.env.GOOGLE_CALLBACK
},
((accessToken, refreshToken, profile, done) => {
  // TODO change this block and user services...
  db.User.findOne({ where: { email: profile.emails[0].value } }).then((userFind) => {
    if (!userFind) {
      db.User.create({
        email: profile.emails[0].value,
        password: Math.random().toString(36).slice(-8)
      })
        .then((userCreated) => {
          done(null, userCreated);
        });
    }
    else {
      done(null, userFind);
    }
  }).catch((err) => done(err));
})));

module.exports = passport;
