
import config from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import passport from 'passport';
import cors from 'cors';
import AuthRoutes from './src/routes/AuthRoutes';
import ProductRoutes from './src/routes/ProductRoutes';
import UserRoutes from './src/routes/UserRoutes';
import db from './src/models';

config.config();

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  secret: process.env.SECRET_KEY || 'secret-key', cookie: { maxAge: 3600000 }, resave: true, saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(cors());

passport.serializeUser((user, done) => {
  done(null, user.id);
});

// TODO CHANGE AND USE SERVICES
passport.deserializeUser((id, done) => {
  db.User.findOne({ exclude: ['id', 'password'], where: { id: Number(id) } }).then((user) => {
    if (user) {
      done(null, user);
    }
    done(null, false);
  });
});

const port = process.env.PORT || 8000;

app.use('/uploads', express.static('public/uploads'));
app.use('/api/v1/users', UserRoutes);
app.use('/api/v1/products', ProductRoutes);
app.use('/api/auth', AuthRoutes);


// when a random route is inputed
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to this API.'
}));

app.listen(port, () => {
  console.log(`Server is running on PORT ${port}`);
});

export default app;
